/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ivan.aplikasi_random;
import java.util.*;
/**
 *
 * @author ivan
 */
public class Main {
    
    final static Random random = new Random();
    
    public static void main(String[] args){
        String name[] = {"Roberto", "Dannys", "Ongthe", "Frendy", "Justin", "Edric", "Ivan"};
        
        System.out.println("Random App");
        System.out.println("===========");
        int randomName = random.nextInt(name.length);
        System.out.println("Congratulations " + name[randomName]);
    }
}
